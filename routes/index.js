const express = require("express");
const router = express.Router();
const jwt = require("../utils/jwt");
const authToken = jwt.authenticateToken;
const testController = require("../controller/testController");
const authController = require("../controller/authController");
const productController = require("../controller/productController");

module.exports = app => {
  router.get("/test", testController.list);

  // Auth
  router.post("/login", authController.login);
  router.post("/register", authController.register);

  // Product
  router.post("/product/create", authToken, productController.createProduct);
  router.get("/product", authToken, productController.listProduct);
  router.get(
    "/product/:product_id",
    authToken,
    productController.getProductById
  );
  router.patch(
    "/product/update/:product_id",
    authToken,
    productController.updateProduct
  );
  router.delete(
    "/product/delete/:product_id",
    authToken,
    productController.deleteProduct
  );

  app.use("/api", router);
};
