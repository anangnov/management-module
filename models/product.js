"use strict";

module.exports = function(sequelize, DataTypes) {
  const user = sequelize.define(
    "product",
    {
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false
      },
      price: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      tableName: "products",
      timestamps: false
    }
  );

  return user;
};
