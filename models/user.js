"use strict";

module.exports = function(sequelize, DataTypes) {
  const user = sequelize.define(
    "user",
    {
      email: {
        type: DataTypes.STRING,
        allowNull: false
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      phone: {
        type: DataTypes.STRING,
        allowNull: false
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      status: {
        type: DataTypes.INTEGER,
        allowNull: true
      }
    },
    {
      tableName: "users",
      timestamps: false
    }
  );

  return user;
};
