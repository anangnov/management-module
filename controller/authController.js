"use-strict";

const async = require("async");
const models = require("../models");
const md5 = require("md5");
const jwt = require("../utils/jwt");

const register = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [
          {
            name: "email",
            value: req.body.email
          },
          {
            name: "name",
            value: req.body.name
          },
          {
            name: "phone",
            value: req.body.phone
          },
          {
            name: "password",
            value: req.body.password
          }
        ];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function checkEmail(data, callback) {
        models.sequelize
          .query("select * from users where email = '" + req.body.email + "'")
          .then((result, err) => {
            if (err)
              return callback({
                error: true,
                message: err,
                data: {}
              });

            if (result[0].length > 0)
              return callback({
                error: true,
                message: "Email was registered",
                data: {}
              });

            callback(null, data);
          })
          .catch(err => {
            callback(null, err);
          });
      },
      function checkPhone(data, callback) {
        models.sequelize
          .query("select * from users where phone = '" + req.body.phone + "'")
          .then((result, err) => {
            if (err)
              return callback({
                error: true,
                message: err,
                data: {}
              });

            if (result[0].length > 0)
              return callback({
                error: true,
                message: "Phone was registered",
                data: {}
              });

            callback(null, data);
          })
          .catch(err => {
            callback(null, err);
          });
      },
      function createData(data, callback) {
        let request = {
          email: req.body.email,
          name: req.body.name,
          phone: req.body.phone,
          status: 1,
          password: md5(req.body.password),
          role_id: req.body.role_id
        };

        models.user.create(request).then((result, err) => {
          if (err)
            return callback({
              error: true,
              message: err,
              data: {}
            });

          callback(null, {
            error: false,
            code: "00",
            message: "Success Register",
            data: {}
          });
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const login = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [
          {
            name: "email",
            value: req.body.email
          },
          {
            name: "password",
            value: req.body.password
          }
        ];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function checkUser(data, callback) {
        let query =
          "select id, email, phone, status from users where email = '" +
          req.body.email +
          "' and password = '" +
          md5(req.body.password) +
          "'";

        models.sequelize.query(query).then((result, err) => {
          console.log(result[0].length);
          if (err)
            return callback({
              error: true,
              message: err,
              data: {}
            });

          if (result[0].length < 1)
            return callback({
              error: true,
              message: "Username or password is wrong",
              data: {}
            });

          let token = jwt.generateAccessToken(req.body);
          result[0][0].token = token;

          callback(null, {
            error: false,
            code: "00",
            message: "Success Login",
            data: result[0][0]
          });
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

module.exports = { login, register };
