"use-strict";

const async = require("async");
const models = require("../models");

const createProduct = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [
          {
            name: "name",
            value: req.body.name
          },
          {
            name: "description",
            value: req.body.description
          },
          {
            name: "price",
            value: req.body.price
          },
          {
            name: "user_id",
            value: req.body.user_id
          }
        ];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function createData(data, callback) {
        models.product.create(req.body).then((result, err) => {
          if (err)
            return callback({
              error: true,
              message: err,
              data: {}
            });

          callback(null, {
            error: false,
            code: "00",
            message: "Success Create",
            data: {}
          });
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const listProduct = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function getData(data, callback) {
        models.product.findAll({}).then((result, err) => {
          if (result[0].length < 1)
            return callback({
              error: true,
              message: "Not Found",
              data: {}
            });

          callback(null, result);
        });
      },
      function final(data, callback) {
        callback(null, {
          error: false,
          message: "Found",
          data: data
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const getProductById = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function getData(data, callback) {
        models.product
          .findAll({
            where: {
              id: req.params.product_id
            }
          })
          .then((result, err) => {
            if (result.length < 1)
              return callback({
                error: true,
                message: "Not Found",
                data: {}
              });

            callback(null, result);
          });
      },
      function final(data, callback) {
        callback(null, {
          error: false,
          message: "Found",
          data: data[0]
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const updateProduct = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function updateData(data, callback) {
        models.product
          .update(
            {
              name: req.body.name,
              description: req.body.description,
              price: req.body.price
            },
            {
              where: {
                id: req.params.product_id
              }
            }
          )
          .then((result, err) => {
            callback(null, {
              error: false,
              message: "Success Update",
              data: data
            });
          });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const deleteProduct = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function getData(data, callback) {
        models.product
          .destroy({
            where: {
              id: req.params.product_id
            }
          })
          .then((result, err) => {
            callback(null, {
              error: false,
              message: "Success Delete",
              data: data
            });
          });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

module.exports = {
  createProduct,
  listProduct,
  getProductById,
  updateProduct,
  deleteProduct
};
